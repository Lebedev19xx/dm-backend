
# Dancemaster 

### 1. Frontend шаблон включает:

- HTML 5 Boilerplate `v 6.2.0`
- Gulp.js `v 3.9.1`
- Bootstrap 4 `v 4.1.3` 
- Jquery `v 3.1`

---

### 2. Backend часть:

- Yii2 framework `v 2.19`

---

### 3. Описание проекта:

Сайт для компании Танцмастер. Компания "DANCEMASTER" является производителем танцевальной обуви и одежды для занятий и выступлений, а также сопутствующих атрибутов танцевальной хореографии. Находясь в постоянном поиске совершенствования изготовления специализированных товаров, мы уделяем значительное внимание комфорту и качеству выпускаемой продукции. Компания "DANCEMASTER" предлагает каталог своих товаров.
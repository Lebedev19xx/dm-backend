<?php

namespace backend\assets;

use yii\web\AssetBundle;
use yii\web\View;

/**
 * Main backend application asset bundle.
 */
class CommonAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/common/style.bundle.css',
        'css/common/base/light.css',
        'css/common/menu/light.css',
        'css/common/brand/dark.css',
        'css/common/aside/dark.css',
        'css/site.css'
    ];
    public $jsOptions = [
      'position' => View::POS_HEAD
    ];
    public $js = [
        'https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js',
        'js/base/scripts.bundle.js',
        'js/bundle/app.bundle.js',
        'js/vendor/bootstrap.bundle.js'
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap4\BootstrapAsset',
    ];
}

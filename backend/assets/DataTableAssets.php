<?php


namespace backend\assets;


use yii\web\AssetBundle;
use yii\web\View;

class DataTableAssets extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/datatables/datatables.bundle.min.css',
    ];
    public $jsOptions = [
        'position' => View::POS_END
    ];
    public $js = [
        'js/base/datatables.bundle.js',
    ];
    public $depends = [
    ];
}
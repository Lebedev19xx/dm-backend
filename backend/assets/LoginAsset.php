<?php
/**
 * Created by PhpStorm.
 * User: dmitrii
 * Date: 26.04.19
 * Time: 11:54
 */

namespace backend\assets;

use yii\web\AssetBundle;
use yii\web\View;


/**
 * loginAsset
 */
class LoginAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/site.css',
        'css/login/login-v1.default.css',
    ];
    public $jsOptions = [
        'position' => View::POS_HEAD
    ];
    public $js = [
        'js/login/login-v1.js',

    ];
    public $depends = [
        'yii\web\YiiAsset',
    ];
}
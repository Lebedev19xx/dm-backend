<?php


namespace backend\controllers;

use yii\web\Controller;

class FilesController extends Controller
{

    public function actions()
    {
        return [
            'upload' => [
                'class' => 'vova07\imperavi\actions\UploadFileAction',
                'url' => 'http://frontend:7888/uploads/images/', // Directory URL address, where files are stored.
                'path' => \Yii::getAlias('@frontend').'/web/uploads/images/', // Or absolute path to directory where files are stored.
            ],
        ];
    }
}
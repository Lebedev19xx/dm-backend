<?php

namespace backend\controllers;

use backend\models\UploadForm;
use common\models\AppFunctions;
use common\models\Images;
use Yii;
use common\models\News;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * NewsController implements the CRUD actions for News model.
 */
class NewsController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all News models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => News::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single News model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new News model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     * @throws \yii\base\Exception
     */
    public function actionCreate()
    {
        $model = new News();

        // Прикрепим поведение с параметрами изображения для последующей обработки
        $img = new UploadForm('News');
        $img->scenario = UploadForm::SCENARIO_CREATE;

        if ($img->load(Yii::$app->request->post())) {

            $img->imageFile = UploadedFile::getInstance($img, 'imageFile');

            $model->create_at = $model->update_at = time();

            if ($model->image_id = $img->upload()) {

                if ($model->load(Yii::$app->request->post())) {

                    $model->link_name = '/news/'.str_replace(AppFunctions::getRus(), AppFunctions::getEng(), $model->title);

                    if ($model->save()) {
                        return $this->redirect(['view', 'id' => $model->id]);

                    }
                }
            }
        }

        return $this->render('create', [
            'model' => $model,
            'img' => $img
        ]);
    }

    /**
     * Updates an existing News model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     * @throws \yii\base\Exception
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        $imageView = Images::findOne($model->image_id);

        // Прикрепим поведение с параметрами изображения для последующей обработки
        $img = new UploadForm('News');

        if ($img->load(Yii::$app->request->post())) {

            $img->imageFile = UploadedFile::getInstance($img, 'imageFile');

            if ($img->imageFile) $model->image_id = $img->upload();
        }

        if ($model->load(Yii::$app->request->post())) {
            $model->update_at = time();

            if ($model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            }
        }

        return $this->render('update', [
            'model' => $model,
            'img' => $img,
            'imageView' => $imageView
        ]);
    }

    /**
     * Deletes an existing News model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the News model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return News the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = News::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}

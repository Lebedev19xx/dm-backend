<?php

namespace backend\controllers;

use backend\models\UploadForm;
use common\models\Images;
use Yii;
use common\models\Slider;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * SliderController implements the CRUD actions for Slider model.
 */
class SliderController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Slider models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Slider::find(),
            'sort' => [
                'defaultOrder' => ['created_at' => SORT_DESC]
            ]
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Slider model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Slider model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     * @throws \yii\base\Exception
     */
    public function actionCreate()
    {
        $model = new Slider();

        // Прикрепим поведение с параметрами изображения для последующей обработки
        $img = new UploadForm('Slider');
        $img->scenario = UploadForm::SCENARIO_CREATE;

        if ($img->load(Yii::$app->request->post())) {

            $img->imageFile = UploadedFile::getInstance($img, 'imageFile');

            if ($model->img_id = $img->upload()) {

                $model->created_at = $model->updated_at = time();
               if ($model->load(Yii::$app->request->post())) {

                   if ($model->save()){
                       return $this->redirect(['view', 'id' => $model->id]);
                   }
               }
            }
        }

        return $this->render('create', [
            'model' => $model,
            'img' => $img
        ]);
    }

    /**
     * Updates an existing Slider model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     * @throws \yii\base\Exception
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $model->scenario = Slider::SCENARIO_UPDATE;

        $imageView = Images::findOne($model->img_id);

        $img = new UploadForm('Slider');

        if ($img->load(Yii::$app->request->post())) {

            $img->imageFile = UploadedFile::getInstance($img, 'imageFile');

            if ($img->imageFile) $model->img_id = $img->upload();
        }

        if ($model->load(Yii::$app->request->post())) {
            $model->updated_at = time();

            if ($model->save()){
                return $this->redirect(['view', 'id' => $model->id]);
            }
        }

        return $this->render('update', [
            'model' => $model,
            'img' => $img,
            'imageView' => $imageView
        ]);
    }

    /**
     * Deletes an existing Slider model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Slider model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Slider the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Slider::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}

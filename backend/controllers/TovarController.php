<?php

namespace backend\controllers;

use backend\models\UploadForm;
use common\models\AppFunctions;
use common\models\Images;
use common\models\MainMenuCategory;
use common\models\News;
use common\models\Subcategory;
use common\models\TovarSearch;
use Yii;
use common\models\Tovar;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;
use yii\web\UploadedFile;

/**
 * TovarController implements the CRUD actions for Tovar model.
 */
class TovarController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }


    /**
     * Lists all Tovar models.
     * @return mixed
     */
    public function actionIndex()
    {
//        $dataProvider = new ActiveDataProvider([
//            'query' => Tovar::find(),
//            'sort' => [
//                'defaultOrder' => ['created_at' => SORT_DESC]
//            ]
//        ]);

        $dataSearch = new TovarSearch();
        $dataProvider = $dataSearch->search(Yii::$app->request->queryParams);
//        $dataProvider->pagination->pageSize = 1;


        $mainCategoryQuery = MainMenuCategory::find()->asArray()->all();
        $mainCategory = ArrayHelper::map($mainCategoryQuery, 'id', 'name');

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'dataSearch' => $dataSearch,
            'mainCategory' => $mainCategory
        ]);
    }

    /**
     *Функция возвращает массив элиментов
     * @param null $filter
     * @return array
     */
    public function actionSubcat($filter = null) {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $out = [];
        if (isset($_POST['depdrop_parents'])) {
            $parents = $_POST['depdrop_parents'];
            if ($parents != null) {
                $cat_id = $parents[0];
                $out = self::getSubCatList($cat_id, $filter);
                // the getSubCatList function will query the database based on the
                // cat_id and return an array like below:
                // [
                //    ['id'=>'<sub-cat-id-1>', 'name'=>'<sub-cat-name1>'],
                //    ['id'=>'<sub-cat_id_2>', 'name'=>'<sub-cat-name2>']
                // ]

                return ['output'=>$out, 'selected'=>''];
            }
        }
        return ['output'=>'', 'selected'=>''];
    }

    /**
     * @param $cat_id
     * @param null $filter
     * @return array|\yii\db\ActiveRecord[]
     */
    public function getSubCatList($cat_id, $filter = null)
    {
        if ($filter) {
            $array = Subcategory::find()->select(['name', 'id'])->where(['main_category_id' => $cat_id])->all();

            foreach ($array as $key => $value) {
                $array[$key]->id = $value->name;
            }
            return $array;
        } else {
            return Subcategory::find()->select(['id', 'name'])->where(['main_category_id' => $cat_id])->all();
        }
    }

    /**
     * Displays a single Tovar model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);

        // Заполняем массив для вывода detailView только заполненных полей
        $view_arr = [];
        foreach ($model as $key => $attr) {
            if ($attr != '') {
                $view_arr[] = $key;
            }
        }


        // Так как массив компонентов формируеться динамически, то мы не можем указать во view как выводить дату,
        // перезапишем поля заранее
        $model->created_at = date('d.m.Y', $model->created_at);
        $model->updated_at = date('d.m.Y', $model->updated_at);

        return $this->render('view', [
            'model' => $model,
        ]);
    }

    /**
     * Creates a new Tovar model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     * @throws \yii\base\Exception
     */
    public function actionCreate()
    {
        $model = new Tovar();
        $model->scenario = Tovar::SCENARIO_CREATE;

        // Прикрепим поведение с параметрами изображения для последующей обработки
        $img = new UploadForm('Product');
        $img->scenario = UploadForm::SCENARIO_CREATE;

        $mainCategoryQuery = MainMenuCategory::find()->asArray()->all();

        $mainCategory = ArrayHelper::map($mainCategoryQuery, 'id', 'name');

        $allSubCategoriesQuery = Subcategory::find()->asArray()->all();
        $allSubCategories = ArrayHelper::map($allSubCategoriesQuery, 'name', 'name');

        if ($img->load(Yii::$app->request->post())) {

            $img->imageFile = UploadedFile::getInstance($img, 'imageFile');
            if ($img->imageFile){
                $model->img_id = $img->upload();
            }
        }

        $model->created_at = $model->updated_at = time();
        if ($model->load(Yii::$app->request->post())) {

            $model->link_name = str_replace(AppFunctions::getRus(), AppFunctions::getEng(), $model->short_name);

            // Если стоит галка создать новость
            if ($model->create_news == 1) {
                $news = new News();
                $news->link_name = '/catalog/' . $model->link_name;
                $news->image_id = $model->img_id;
                $news->title = $model->short_name;
                $news->text = $model->description;
                $news->create_at = $news->update_at = time();
                $news->save();
            }

            // Найдем имя категории и перезапишем переменную
            $model->subcategory_name = Subcategory::findOne($model->subcategory_name)->name;

            if ($model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            }
        }

        return $this->render('create', [
            'model' => $model,
            'subcategory' => $allSubCategories,
            'mainCategory' => $mainCategory,
            'img' => $img
        ]);
    }

    /**
     * Updates an existing Tovar model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     * @throws \yii\base\Exception
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        // Прикрепим поведение с параметрами изображения для последующей обработки
        $img = new UploadForm('Product');

        $imageView = Images::findOne($model->img_id);

        $mainCategoryQuery = MainMenuCategory::find()->asArray()->all();

        $mainCategory = ArrayHelper::map($mainCategoryQuery, 'id', 'name');

        $allSubCategoriesQuery = Subcategory::find()->asArray()->all();
        $allSubCategories = ArrayHelper::map($allSubCategoriesQuery, 'name', 'name');

        // Загрузка изображения
        if ($img->load(Yii::$app->request->post())) {
            $img->imageFile = UploadedFile::getInstance($img, 'imageFile');

            if ($img->imageFile){
                $model->img_id = $img->upload();
            }
        }

        $model->updated_at = time();
        if ($model->load(Yii::$app->request->post())) {

            $model->link_name = str_replace(AppFunctions::getRus(), AppFunctions::getEng(), $model->short_name);

            if (mb_strlen($model->subcategory_name) < 3) {
                // Найдем имя категории и перезапишем переменную
                $model->subcategory_name = Subcategory::findOne($model->subcategory_name)->name;
            }

            if ($model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            }
        }

        return $this->render('update', [
            'model' => $model,
            'subcategory' => $allSubCategories,
            'mainCategory' => $mainCategory,
            'img' => $img,
            'imageView' => $imageView
        ]);
    }

    /**
     * Изменияем sort
     * @param $id
     * @throws NotFoundHttpException
     */
    public function actionSort($id){
        $model = $this->findModel($id);
        $model->sort = Yii::$app->request->post('sort');
        $model->save();
    }

    /**
     * Deletes an existing Tovar model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Tovar model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Tovar the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Tovar::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}

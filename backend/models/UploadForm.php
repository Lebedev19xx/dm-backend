<?php
namespace backend\models;

use common\models\Images;
use Imagick;
use Imagine\Image\Box;
use Imagine\Image\Point;
use Yii;
use yii\base\Model;
use yii\helpers\FileHelper;
use yii\helpers\Inflector;
use yii\imagine\Image;
use yii\web\UploadedFile;

class UploadForm extends Model
{
    /**
     * @var UploadedFile
     */
    public $imageFile;
    public $group;
    const SCENARIO_CREATE = 'create';

    public function rules()
    {
        return [
            [['imageFile'], 'required', 'on' => self::SCENARIO_CREATE],
            [['imageFile'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg'],
        ];
    }

    /**
     * Присоединяет поведение по имени группы
     *
     * @param $group string Группа файла
     */
    public function __construct($group)
    {
        $this->group = $group;

        if($this->group) {
            // Для подключения группы нужно знать название класса. Его можно сформировать из названия группы, переведя
            // его в CamelCase
            $fileGroup = Inflector::camelize($this->group);

            // Подключить поведение группы
            $this->attachBehavior($fileGroup, 'common\\components\\filesGroups\\' . $fileGroup);
        }
    }

    /**
     * Обработка изображения (если это изображение)
     * @throws \Exception
     */
    public function afterValidate()
    {
        if (!$this->hasErrors()) {

            /**
             * Если ширина или высота загружаемого изображения больше,
             * чем ширина или высота установленная подключенным валидатором, то сжимаем изображение в установленных пределах
             **/
            $imagine = new Image();
            $photo = $imagine::getImagine()->open($this->imageFile->tempName);

            $width = $photo->getSize()->getWidth();
            $height = $photo->getSize()->getHeight();

            // Установить качество сохраняемого изображения
            $quality = 70;

            if (($width >= $this->getMaxWidth()) || ($height >= $this->getMaxHeight())) {

                switch ($this->getTypeOptimize()) {
                    case 'crop':

                        // Записать пределы разрешений в переменные, одна из которых далее будет изменена
                        $limitWidth = $this->getMaxWidth();
                        $limitHeight = $this->getMaxHeight();

                        // Высчитать пропорции для сжатия
                        $k1 = $width / $height;             // Пропорция "До"
                        $k2 = $limitWidth / $limitHeight;   // Пропорция после
                        if ($k1 > $k2) {
                            $limitWidth = round($this->getMaxHeight() * $k1);
                        } else {
                            $limitHeight = round($this->getMaxWidth() / $k1);
                        }

                        // Уменьшить фото до заданных размеров
                        $photo->resize(new Box($limitWidth, $limitHeight));

                        // Crop по центру изображения
                        $photo->crop(new Point(
                            ($limitWidth - $this->getMaxWidth()) / 2,
                            ($limitHeight - $this->getMaxHeight()) / 2),
                            new Box($this->getMaxWidth(), $this->getMaxHeight()));

                        // Сохранить изображение во временную папку
                        $photo->save($this->imageFile->tempName);

                        break;

                    case 'resize':
                        $photo->resize(new Box($this->getMaxWidth(), $this->getMaxHeight()))
                            ->save($this->imageFile->tempName);
                        break;

                    default:

                        // Сделать ресайз
                        $photo->resize(new Box($this->getMaxWidth(), $this->getMaxHeight()))
                            ->save($this->imageFile->tempName);
                }

                // Оптимизировать качество
                $im = new Imagick($this->imageFile->tempName);

                $im->optimizeImageLayers();
                $im->setImageCompression(Imagick::COMPRESSION_JPEG);
                $im->setImageCompressionQuality($quality);

                // Сохраняем
                $im->writeImages($this->imageFile->tempName, true);
            }
        }

        return parent::afterValidate();
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'imageFile' => Yii::t('app', 'Изображение')
        ];
    }

    /**
     * Функция обработки и загрузки изображения
     * @return bool|int
     * @throws \yii\base\Exception
     */
    public function upload()
    {
        // Если файл загружен и провалидирован
        if ($this->imageFile && $this->validate()) {

            $images = new Images();
            $images->name = base_convert(md5_file($this->imageFile->tempName), 16, 36);
            $images->ext = $this->imageFile->extension;

            $i = 0;
            // Условие сравнивается с false, потому что секция может оказаться нулем
            while (substr($images->name, 2 * ($i + 1), 2) !== false) {
                $images->path .= '/' . substr($images->name, 2 * $i, 2);
                ++$i;
            }

            // Если такой файл уже есть на сервере, тогда найдем его и вернем id
            if (file_exists(Yii::getAlias('@frontend') . '/web/uploads'
                . $images->path . '/' . $images->name . '.' . $this->imageFile->extension)){
                $image = Images::find()->where(['path' => $images->path])->one();
                return $image->id;
            } else {
                if ($this->validate() && $images->save()) {

                    FileHelper::createDirectory(Yii::getAlias('@frontend') . '/web/uploads' .
                        $images->path);
                    $this->imageFile->saveAs(Yii::getAlias('@frontend') . '/web/uploads'
                        . $images->path . '/' . $images->name . '.' . $this->imageFile->extension);

                    return $images->id;
                } else {
                    return false;
                }
            }
        }
    }
}
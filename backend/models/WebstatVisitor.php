<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "webstat_visitor".
 *
 * @property int $id
 * @property string $cookie_id
 * @property int $user_id
 * @property int $source
 * @property string $ip_address
 * @property string $url
 * @property string $referrer
 * @property string $user_agent
 * @property string $created_at
 */
class WebstatVisitor extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'webstat_visitor';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['cookie_id', 'ip_address', 'url'], 'required'],
            [['user_id', 'source'], 'integer'],
            [['created_at'], 'safe'],
            [['cookie_id'], 'string', 'max' => 32],
            [['ip_address'], 'string', 'max' => 15],
            [['url', 'referrer', 'user_agent'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'cookie_id' => Yii::t('app', 'Cookie ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'source' => Yii::t('app', 'Source'),
            'ip_address' => Yii::t('app', 'Ip Address'),
            'url' => Yii::t('app', 'Url'),
            'referrer' => Yii::t('app', 'Referrer'),
            'user_agent' => Yii::t('app', 'User Agent'),
            'created_at' => Yii::t('app', 'Created At'),
        ];
    }

    /**
     * {@inheritdoc}
     * @return WebstatVisitorQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new WebstatVisitorQuery(get_called_class());
    }
}

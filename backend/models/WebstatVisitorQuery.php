<?php

namespace backend\models;

/**
 * This is the ActiveQuery class for [[WebstatVisitor]].
 *
 * @see WebstatVisitor
 */
class WebstatVisitorQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return WebstatVisitor[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return WebstatVisitor|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}

<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Footer */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="footer-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'text')->textarea(['rows' => 6]) ?>

    <?php if (isset($imageView)):?>
        <img src="<?= 'http://cc59539.tmweb.ru/uploads' . $imageView->path .'/'. $imageView->name .'.'. $imageView->ext;?>"
             width="150" height="150" alt="">
    <?php endif;?>

    <?= $form->field($img, 'imageFile')->fileInput()->label('Изображение') ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

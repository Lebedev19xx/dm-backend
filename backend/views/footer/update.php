<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Footer */

$this->title = 'Update Footer: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Footers', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="footer-update">

    <div class="kt-portlet ">
        <div class="kt-widget14">
            <div class="kt-widget14__header">
                <h3 class="kt-widget14__title">
                    <?= Html::encode($this->title) ?>
                </h3>
            </div>
            <div class="kt-widget14__content">
                <div style="width: 100%">
                <?= $this->render('_form', [
                    'model' => $model,
                    'img' => $img,
                    'imageView' => $imageView
                ]) ?>
                </div>
            </div>
        </div>
    </div>
</div>

<?php
use backend\assets\CommonAsset;
use backend\assets\LoginAsset;
use yii\bootstrap4\Html;

CommonAsset::register($this);
LoginAsset::register($this);

?>
<?php $this->beginPage() ?>
    <!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
<!--        <link rel="apple-touch-icon" sizes="180x180" href="https://static.tonkosti.ru/img/favicon/apple-touch-icon.png">-->
<!--        <link rel="icon" type="image/png" sizes="32x32" href="https://static.tonkosti.ru/img/favicon/favicon-32x32.png">-->
<!--        <link rel="icon" type="image/png" sizes="16x16" href="https://static.tonkosti.ru/img/favicon/favicon-16x16.png">-->
<!--        <link rel="manifest" href="https://static.tonkosti.ru/img/favicon/manifest.json">-->
<!--        <link rel="mask-icon" href="https://static.tonkosti.ru/img/favicon/safari-pinned-tab.svg" color="#05769e">-->
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">

        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <?php $this->head() ?>

        <!--begin::Fonts -->
        <script>
            WebFont.load({
                google: {
                    "families": ["Poppins:300,400,500,600,700", "Roboto:300,400,500,600,700"]
                },
                active: function() {
                    sessionStorage.fonts = true;
                }
            });
        </script>

        <!--end::Fonts -->
    </head>

    <!-- end::Head -->

    <!-- begin::Body -->
    <body class="kt-header--fixed kt-header-mobile--fixed kt-subheader--fixed kt-subheader--enabled kt-subheader--solid kt-aside--enabled kt-aside--fixed kt-page--loading">

    <?php $this->beginBody() ?>
    <!-- begin:: Page -->
    <div class="kt-grid kt-grid--ver kt-grid--root">
        <div class="kt-grid kt-grid--hor kt-grid--root  kt-login kt-login--v1" id="kt_login">
            <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--desktop kt-grid--ver-desktop kt-grid--hor-tablet-and-mobile">

                <!--begin::Aside-->
                <div class="kt-grid__item kt-grid__item--order-tablet-and-mobile-2 kt-grid kt-grid--hor kt-login__aside" style="background-image: url(/backend/web/img/bg-4.jpg);">
                    <div class="kt-grid__item">
                        <a href="#" class="kt-login__logo">
                            <img src="/backend/web/img/logo-4.png">
                        </a>
                    </div>
                    <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--ver">
                        <div class="kt-grid__item kt-grid__item--middle">
                            <h3 class="kt-login__title">Добро пожаловать в панель управления!</h3>
                            <h4 class="kt-login__subtitle">Управляйте сайтом, следите за статистикой!</h4>
                        </div>
                    </div>
                    <div class="kt-grid__item">
<!--                        <div class="kt-login__info">-->
<!--                            <div class="kt-login__copyright">-->
<!--                                &copy 2018 Metronic-->
<!--                            </div>-->
<!--                            <div class="kt-login__menu">-->
<!--                                <a href="#" class="kt-link">Privacy</a>-->
<!--                                <a href="#" class="kt-link">Legal</a>-->
<!--                                <a href="#" class="kt-link">Contact</a>-->
<!--                            </div>-->
<!--                        </div>-->
                    </div>
                </div>
                <?= $content?>
            </div>
        </div>
    </div>

    <!-- end:: Page -->

    <!-- begin::Global Config(global config for global JS sciprts) -->
    <script>
      var KTAppOptions = {
        "colors": {
          "state": {
            "brand": "#5d78ff",
            "dark": "#282a3c",
            "light": "#ffffff",
            "primary": "#5867dd",
            "success": "#34bfa3",
            "info": "#36a3f7",
            "warning": "#ffb822",
            "danger": "#fd3995"
          },
          "base": {
            "label": ["#c5cbe3", "#a1a8c3", "#3d4465", "#3e4466"],
            "shape": ["#f0f3ff", "#d9dffa", "#afb4d4", "#646c9a"]
          }
        }
      };
    </script>

<?php $this->endBody() ?>
    </body>
</html>
<?php $this->endPage() ?>

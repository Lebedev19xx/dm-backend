<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Главные категории');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="main-menu-category-index">

    <?php Pjax::begin(); ?>

    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__head kt-portlet__head--lg">
            <div class="kt-portlet__head-label">
                <span class="kt-portlet__head-icon">
                    <i class="kt-font-brand flaticon2-line-chart"></i>
                </span>
                <h3 class="kt-portlet__head-title">
                    <?= Html::encode($this->title);?>
                </h3>
            </div>
            <div class="kt-portlet__head-toolbar">
                <div class="kt-portlet__head-wrapper">
                    <?= Html::a(Yii::t('app',
                        '<i class="la la-long-arrow-left"></i> Назад'), Yii::$app->request->referrer,
                        ['class' => 'btn btn-clean btn-icon-sm']) ?>
                    <div class="dropdown dropdown-inline">
                        <?= Html::a(Yii::t('app',
                            '<i class="flaticon2-plus"></i> Довавить категорию'), ['create'],
                            ['class' => 'btn btn-brand btn-icon-sm']) ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="kt-portlet__body">
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],

//                    'id',
                    'name',

                    ['class' => 'yii\grid\ActionColumn',
                        'template' => '<div class="dropdown">
                                      <a class="btn-lg" style="color: #0f0f0f; cursor: pointer;" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                        <i class="fas fa-ellipsis-v"></i>
                                      </a>
                                      <ul class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenu1">
                                        {view}
                                        {update}
                                        <div class="dropdown-divider"></div>
                                        {delete}
                                      </ul>
                                    </div>',
                        'buttons' => [
                            'view' => function ($url) {
                                return Html::a(
                                    '<i class="far fa-eye"></i> '
                                    . Yii::t('app', 'Просмотр'),
                                    $url,
                                    [
                                        'title' => Yii::t('app', 'Просмотр'),
                                        'data-pjax' => '0',
                                        'class' => 'dropdown-item',
                                    ]
                                );
                            },
                            'update' => function ($url) {
                                return Html::a(
                                    '<i class="fas fa-pencil-alt"></i> '
                                    . Yii::t('app', 'Редактировать'),
                                    $url,
                                    [
                                        'title' => Yii::t('app', 'Обновить'),
                                        'data-pjax' => '0',
                                        'class' => 'dropdown-item',
                                    ]
                                );
                            },
                            'delete' => function ($url) {
                                return Html::a(
                                    '<i class="fas fa-trash"></i> ' .
                                    Yii::t('app', 'Удалить'),
                                    $url,
                                    [
                                        'title' => Yii::t('app', 'Удалить'),
                                        'data-pjax' => '0',
                                        'class' => 'dropdown-item',
                                        'data' => [
                                            'confirm' => Yii::t('app',
                                                'Вы уверены, что хотите удалить?'),
                                            'method' => 'post',
                                        ],
                                    ]
                                );
                            }
                        ],
                    ],
                ],
            ]); ?>

            <?php Pjax::end(); ?>
        </div>

    </div>

</div>



<?php

use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\News */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="news-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'text')->textarea(['rows' => 6]) ?>

    <?php if (isset($imageView)):?>
        <img src="<?= 'http://cc59539.tmweb.ru/uploads' . $imageView->path .'/'. $imageView->name .'.'. $imageView->ext;?>"
             width="150" height="150" alt="">
    <?php endif;?>
    
    <?= $form->field($img, 'imageFile')->fileInput()->label('Изображение') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Сохранить'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

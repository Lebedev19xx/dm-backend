<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\News */

$this->title = Yii::t('app', 'Добавить новость');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Управление новостями'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="news-create">

    <div class="kt-portlet ">
        <div class="kt-widget14">
            <div class="kt-widget14__header">
                <h3 class="kt-widget14__title">
                    <?= Html::encode($this->title) ?>
                </h3>
            </div>
            <div class="kt-widget14__content">
                <div style="width: 100%">

                <?= $this->render('_form', [
                    'model' => $model,
                    'img' => $img
                ]) ?>
                </div>
            </div>
        </div>
    </div>
</div>

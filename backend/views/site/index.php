<?php

/* @var $this yii\web\View */


$this->title = 'Управление сайтом';

use common\widgets\StatSevenDaysWidget; ?>
<div class="kt-portlet ">
    <div class="kt-widget14">
        <div class="kt-widget14__header d-flex justify-content-between">
            <div class="widget14__header--one ">
                <h3 class="kt-widget14__title">
                    Статистика уникальных посещений
                </h3>
                <span class="kt-widget14__desc">За последние 7 дней</span>
            </div>
            <div class="widget14__header--two">
                <h3 class="kt-widget14__title">Более подробная статистика</h3>
                <a href="https://analytics.google.com/analytics/web/?authuser=1#/report-home/a140443718w201373036p195356065" target="_blank">
                    <span class="google-analytics-logo"></span>
                </a>
                <a href="https://metrika.yandex.ru/dashboard?group=day&period=week&id=53694577" target="_blank">
                    <span class="ya-metrika-logo"></span>
                </a>
            </div>
        </div>
        <div class="kt-widget14__content">
            <?= StatSevenDaysWidget::widget()?>
        </div>
    </div>
</div>



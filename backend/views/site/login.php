<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;

$this->title = 'Login';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="kt-grid__item kt-grid__item--fluid  kt-grid__item--order-tablet-and-mobile-1  kt-login__wrapper">

    <!--begin::Head-->
<!--    <div class="kt-login__head">-->
<!--        <span class="kt-login__signup-label">Don't have an account yet?</span>&nbsp;&nbsp;-->
<!--        <a href="#" class="kt-link kt-login__signup-link">Sign Up!</a>-->
<!--    </div>-->

    <div class="kt-login__body">

        <!--begin::Signin-->
        <div class="kt-login__form">
            <div class="kt-login__title">
                <h3>Войдите</h3>
            </div>

            <!--begin::Form-->

            <?php $form = ActiveForm::begin(['id' => 'login-form', 'class' => 'kt-form']);?>

            <?= $form->field($model, 'username')
                ->textInput(['autofocus' => true, 'placeholder' => 'Имя пользователя'])
                ->label(false)?>

            <?= $form->field($model, 'password')
                ->passwordInput(['placeholder' => 'Пароль'])
                ->label(false)?>

            <?= $form->field($model, 'rememberMe')->checkbox()?>

            <!--begin::Action-->
            <div class="kt-login__actions">
                <a href="#" class="kt-link kt-login__link-forgot">
                    Забыли пароль ?
                </a>
                <?= Html::submitButton('Войти', ['class' => 'btn btn-primary btn-elevate kt-login__btn-primary'])?>
            </div>

            <?php ActiveForm::end();?>

            <!--end::Form-->

            <!--begin::Divider-->
            <div class="kt-login__divider">
                <div class="kt-divider">
                    <span></span>
                    <span>Или</span>
                    <span></span>
                </div>
            </div>

            <!--end::Divider-->

            <!--begin::Options-->
            <div class="kt-login__options">
                <a href="#" class="btn btn-primary kt-btn">
                    <i class="fab fa-facebook-f"></i>
                    Facebook
                </a>
                <a href="#" class="btn btn-info kt-btn">
                    <i class="fab fa-twitter"></i>
                    Twitter
                </a>
                <a href="#" class="btn btn-danger kt-btn">
                    <i class="fab fa-google"></i>
                    Google
                </a>
            </div>

            <!--end::Options-->
        </div>

        <!--end::Signin-->
    </div>

    <!--end::Body-->
</div>

<!--end::Content-->
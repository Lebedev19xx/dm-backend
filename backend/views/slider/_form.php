<?php

use common\models\Slider;
use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Slider */
/* @var $form yii\widgets\ActiveForm */
?>


<div class="slider-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'short_title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'text')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'btn_link')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'slide_position')->radioList([0 => 'Левая', 1 => 'Правая'])?>

    <?= $form->field($img, 'imageFile')->fileInput()->label('Изображение') ?>

    <?php if ($model->scenario == Slider::SCENARIO_UPDATE):?>
        <img src="<?= 'http://cc59539.tmweb.ru/uploads' . $imageView->path .'/'. $imageView->name .'.'. $imageView->ext;?>"
             width="150" height="150" alt="">
    <?php endif;?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Сохранить'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>


<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Subcategory */

$this->title = Yii::t('app', 'Создать подкатегории');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Подкатегории'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="subcategory-create">
        <div class="kt-portlet ">
            <div class="kt-widget14">
                <div class="kt-widget14__header">
                    <h3 class="kt-widget14__title">
                        <?= Html::encode($this->title) ?>
                    </h3>
                </div>
                <div class="kt-widget14__content">
                    <div style="width: 100%">

                    <?= $this->render('_form', [
                        'model' => $model,
                        'mainCategory' => $mainCategory,
                    ]) ?>
                    </div>
                </div>
            </div>
        </div>
</div>

<?php

use yii\helpers\Html;
use yii\web\YiiAsset;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Subcategory */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Подкатегории'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
YiiAsset::register($this);
?>
<div class="subcategory-view">

    <div class="kt-portlet ">
        <div class="kt-widget14">
            <div class="kt-widget14__header">
                <h3 class="kt-widget14__title">
                    <?= Html::encode($this->title) ?>
                </h3>
            </div>
            <div class="kt-widget14__content">
                <div style="width: 100%">

                <p>
                    <?= Html::a(Yii::t('app', 'Редактировать'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
                    <?= Html::a(Yii::t('app', 'Удалить'), ['delete', 'id' => $model->id], [
                        'class' => 'btn btn-danger',
                        'data' => [
                            'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                            'method' => 'post',
                        ],
                    ]) ?>
                </p>

                <?= DetailView::widget([
                    'model' => $model,
                    'attributes' => [
                        'id',
                        'name',
                        'url_name:url',
                        'main_category_id',
                        'sort',
                        'text'
                    ],
                ]) ?>
                </div>
            </div>
        </div>
    </div>
</div>

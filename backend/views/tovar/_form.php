<?php

use common\models\Images;
use common\models\Tovar;
use dosamigos\ckeditor\CKEditor;
use kartik\depdrop\DepDrop;
use vova07\imperavi\Widget;
use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\Tovar */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tovar-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'short_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'sort')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'main_category')->dropDownList($mainCategory,
        ['id' => 'mainCat', 'prompt'=>'Выберете категорию'])->label('Главная категория')?>

    <?= $form->field($model, 'subcategory_name')->widget(DepDrop::classname(), [
        'options'=>['id'=>'subcat-id'],
        'pluginOptions'=>[
            'depends'=>['mainCat'],
            'placeholder'=>'Выберете подкатегорию',
            'url' => Url::to(['/tovar/subcat'])
        ]
    ]); ?>
    <?php if ($model->scenario !== Tovar::SCENARIO_CREATE):?>
    <button type="button" id="change-cat" class="btn btn-primary">Изменить категории</button>
    <?php endif;?>

    <?php
    echo $form->field($model, 'item_values')->widget(Widget::className(), [
        'settings' => [
            'lang' => 'ru',
            'minHeight' => 200,
            'imageUpload' => Url::to(['/files/upload']),
            'plugins' => [
                'clips',
                'fullscreen',
                'imagemanager',
            ],
            'clips' => [
                ['Текст в квадрате', '<span class="label-red">Текст в квадрате</span>'],
            ],
        ],
    ]);?>
    <?php
    echo $form->field($model, 'description')->widget(Widget::className(), [
        'settings' => [
            'lang' => 'ru',
            'minHeight' => 200,
            'imageUpload' => Url::to(['/files/upload']),
            'plugins' => [
                'clips',
                'fullscreen',
                'imagemanager',
            ],
            'clips' => [
                ['Текст в квадрате', '<span class="label-red">Текст в квадрате</span>'],
            ],
        ],
    ]);?>
    <?= $form->field($model, 'create_news',  ['inputOptions' => ['class' => 'form-group others'],
            'labelOptions' => [ 'class' => 'custom-control-label others']])->checkbox(['class' => 'custom-control-input others',
            'template'=> '<div class="custom-control custom-switch">{input}{label}</div>'])?>

    <?php if ($model->scenario !== Tovar::SCENARIO_CREATE):?>
    <img src="<?= 'http://cc59539.tmweb.ru/uploads' . $imageView->path .'/'. $imageView->name .'.'. $imageView->ext;?>"
         width="150" height="150" alt="">
    <?php endif;?>
    <?= $form->field($img, 'imageFile', ['options' => ['class' => 'form-group others']])->fileInput() ?>

    <div class="form-group others">
        <?= Html::submitButton(Yii::t('app', 'Сохранить'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end();?>


    <?php if ($model->scenario == Tovar::SCENARIO_CREATE):?>
    <script>
        let select = $('#subcat-id');
        let others = $('.others');
        let i = 0;


        select.on('change', function () {
            let selected = $('#subcat-id option:selected');
            if (selected.text()){

                $.each(others, function (index, value) {
                    value.style = 'display:block';
                });
            }

            if (!selected.val()){
                $.each(others, function (index, value) {
                    value.style = 'display:none';
                });
            }

        });
    </script>
    <?php else:?>
        <style> .others{display: block;} </style>
        <script>
            let btnChange = $('#change-cat');
            let selectSub = $('.field-mainCat').hide();
            let selectMain = $('.field-subcat-id').hide();

            btnChange.on('click', function () {
                this.style = 'display:none';
                selectSub.show();
                selectMain.show();
            });
        </script>
    <?php endif;?>
</div>

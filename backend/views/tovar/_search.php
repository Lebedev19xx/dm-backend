<?php

use kartik\depdrop\DepDrop;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\TovarSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tovar-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'main_category')->dropDownList($mainCategory,
        ['id' => 'mainCat', 'prompt'=>'Выберете категорию'])->label('Главная категория')?>

    <?= $form->field($model, 'subcategory_name')->widget(DepDrop::classname(), [
        'options'=>['id'=>'subcat-id'],
        'pluginOptions'=>[
            'depends'=>['mainCat'],
            'placeholder'=>'Выберете подкатегорию',
            'url' => Url::to(['/tovar/subcat?filter=true'])
        ]
    ]); ?>

    <div class="form-group">
        <?= Html::submitButton('Найти', ['class' => 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

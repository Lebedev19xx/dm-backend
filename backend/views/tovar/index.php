<?php

use backend\assets\DataTableAssets;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Товары');
$this->params['breadcrumbs'][] = $this->title;
DataTableAssets::register($this);
?>
<div class="tovar-index">
    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__head kt-portlet__head--lg">
            <div class="kt-portlet__head-label">
                <span class="kt-portlet__head-icon">
                    <i class="kt-font-brand flaticon2-line-chart"></i>
                </span>
                <h3 class="kt-portlet__head-title">
                    <?= Html::encode($this->title);?>
                </h3>
            </div>
            <div class="kt-portlet__head-toolbar">
                <div class="kt-portlet__head-wrapper">
                    <?= Html::a(Yii::t('app',
                        '<i class="la la-long-arrow-left"></i> Назад'), Yii::$app->request->referrer,
                        ['class' => 'btn btn-clean btn-icon-sm']) ?>
                    <div class="dropdown dropdown-inline">
                        <?= Html::a(Yii::t('app',
                            '<i class="flaticon2-plus"></i> Довавить товар'), ['create'],
                            ['class' => 'btn btn-brand btn-icon-sm']) ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="kt-portlet__body">
    <?php Pjax::begin(); ?>

    <?= $this->render('_search', ['model' => $dataSearch, 'mainCategory' => $mainCategory])?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $dataSearch,
        'tableOptions' => [
//            'id' => 'kt_table_1',
            'class' => 'table table-striped- table-bordered table-hover table-checkable'
        ],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

//            'id',
            [
                'attribute' => 'name',
                'value' => function ($model) {
                    return Html::a($model->name, Url::to(['view', 'id' => $model->id]));
                },
                'format' => 'raw',
            ],
            'subcategory_name',
            [
                'attribute' => 'sort',
                'value' => function ($model) {
                    return Html::input('text', 'sort', $model->sort, ['class' => 'sort form-control', 'data-id' => $model->id]);
                },
                'format' => 'raw'
            ],
            [
                'attribute' => 'created_at',
                'value' => function ($model){
                    return date('d.m.Y', $model->created_at);

                }
            ],

            ['class' => 'yii\grid\ActionColumn',
                'template' => '<div class="dropdown">
                                      <a class="btn-lg" style="color: #0f0f0f; cursor: pointer;" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                        <i class="fas fa-ellipsis-v"></i>
                                      </a>
                                      <ul class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenu1">
                                        {view}
                                        {update}
                                        <div class="dropdown-divider"></div>
                                        {delete}
                                      </ul>
                                    </div>',
                'buttons' => [
                    'view' => function ($url) {
                        return Html::a(
                            '<i class="far fa-eye"></i> '
                            . Yii::t('app', 'Просмотр'),
                            $url,
                            [
                                'title' => Yii::t('app', 'Просмотр'),
                                'data-pjax' => '0',
                                'class' => 'dropdown-item',
                            ]
                        );
                    },
                    'update' => function ($url) {
                        return Html::a(
                            '<i class="fas fa-pencil-alt"></i> '
                            . Yii::t('app', 'Редактировать'),
                            $url,
                            [
                                'title' => Yii::t('app', 'Обновить'),
                                'data-pjax' => '0',
                                'class' => 'dropdown-item',
                            ]
                        );
                    },
                    'delete' => function ($url) {
                        return Html::a(
                            '<i class="fas fa-trash"></i> ' .
                            Yii::t('app', 'Удалить'),
                            $url,
                            [
                                'title' => Yii::t('app', 'Удалить'),
                                'data-pjax' => '0',
                                'class' => 'dropdown-item',
                                'data' => [
                                    'confirm' => Yii::t('app',
                                        'Вы уверены, что хотите удалить?'),
                                    'method' => 'post',
                                ],
                            ]
                        );
                    }
                ],
            ],
        ],
    ]); ?>
        </div>
    <?php Pjax::end(); ?>

</div>
    <script>
        jQuery(function($) {
            $('.sort').on('change', function () {
                $.post('/tovar/sort?id=' + $(this).data('id'),{
                    sort: $(this).val()
                }, function (date) {

                })
            })
        });
    </script>
    <script>
        $(document).ready(function() {
            $('#kt_table_1').DataTable({
                "language": {
                    "lengthMenu": "Записей на странице _MENU_",
                    "zeroRecords": "Записей не найдено",
                    "info": "Страница _PAGE_ из _PAGES_",
                    "infoEmpty": "Нет записей",
                    "infoFiltered": "(filtered from _MAX_ total records)",
                    "search" : "Поиск"
                }
            });

        } );
    </script>
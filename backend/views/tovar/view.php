<?php

use common\models\Images;
use yii\helpers\Html;
use yii\web\YiiAsset;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Tovar */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Товары'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
YiiAsset::register($this);
?>
<div class="tovar-view">
    <div class="kt-portlet ">
        <div class="kt-widget14">
            <div class="kt-widget14__header">
                <h3 class="kt-widget14__title">
                    <?= Html::encode($this->title) ?>
                </h3>
            </div>
            <div class="kt-widget14__content">
                <div style="width: 100%">

                <p>
                    <?= Html::a(Yii::t('app', 'Редактировать'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
                    <?= Html::a(Yii::t('app', 'Удалить'), ['delete', 'id' => $model->id], [
                        'class' => 'btn btn-danger',
                        'data' => [
                            'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                            'method' => 'post',
                        ],
                    ]) ?>
                </p>

                <?= DetailView::widget([
                    'model' => $model,
                    'attributes' => [
                        'name',
                        'short_name',
                        'subcategory_name',
                        'item_values',
                        'description',
                        'sort',
                        [
                            'attribute' => 'img_id',
                            'value' => function ($model) {
                                $image = Images::findOne($model->img_id);
                                return 'http://cc59539.tmweb.ru/uploads' . $image->path .'/'. $image->name .'.'. $image->ext;
                            },
                            'format' => ['image',['width'=>'100','height'=>'100']],
                        ],
                        'updated_at',
                        'created_at'
                    ],
                ]) ?>
                </div>
            </div>
        </div>
    </div>
</div>

<?php

/**
 * Поведение предоставляет набор свойств и правил валидации для файлов карточки продукта
 */

namespace common\components\filesGroups;

use yii\base\Behavior;


class Slider extends Behavior
{
    /**
     * @return string Название группы
     */
    public static function group()
    {
        return 'Slider';
    }

    /**
     * @return integer Размер каждого файла в байтах
     */
    public function getMaxFileSize()
    {
        return 5242880;  // 5 Мб
    }

    /**
     * @return array
     */
    public function getMime()
    {
        return [
            'image/jpeg',
            'image/png',
        ];
    }

    /**
     * @return integer Min высота картинки в пикселях
     */
    public function getMinWidth()
    {
        return 400;
    }

    /**
     * @return integer Min ширина картинки в пикселях
     */
    public function getMinHeight()
    {
        return 400;
    }

    /**
     * @return integer Max ширина картинки в пикселях
     */
    public function getMaxWidth()
    {
        return 1920;
    }

    /**
     * @return integer Max высота картинки в пикселях
     */
    public function getMaxHeight()
    {
        return 1080;
    }

    /**
     * @return string Вариант обработки изображения
     */
    public function getTypeOptimize()
    {
        return 'crop';
    }
}
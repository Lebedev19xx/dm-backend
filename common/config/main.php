<?php

return [
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
    ],
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'components' => [
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
    ],
    'modules' => [
        'stat' => [
            'class' => akiraz2\stat\Module::class,
            'yandexMetrika' => [ // false by default
                'id' => 53694577,
                'params' => [
                    'clickmap' => true,
                    'trackLinks' => true,
                    'accurateTrackBounce' => true,
                    'webvisor' => true
                ]
            ],
            'googleAnalytics' => [ // false by default
                'id' => 'UA-140443718-1',
            ],
            'ownStat' => true, //false by default
            'ownStatCookieId' => 'yii2_counter_id', // 'yii2_counter_id' default
            'onlyGuestUsers' => true, // true default
            'countBot' => false, // false default
            'appId' => ['dancemaster'], // by default count visits only from Frontend App (in backend app we dont need it)
            'blackIpList' => [], // ['127.0.0.1'] by default
        ],
     ],
];

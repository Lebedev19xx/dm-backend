<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "footer".
 *
 * @property int $id
 * @property string $text
 * @property int $img_id
 */
class Footer extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'footer';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['text'], 'string'],
            [['img_id'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'text' => 'Текст',
            'img_id' => 'Изображение',
        ];
    }

    /**
     * Получить изображение
     * @return string
     */
    public function getImage()
    {
        /** @var Images $image */
        $image = Images::find()->where(['id' => $this->img_id])->one();

        if ($image) {
            return '/uploads' . $image->path .'/'. $image->name .'.'. $image->ext;
        } else {
            return false;
        }
    }
}

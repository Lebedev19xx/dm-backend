<?php

namespace common\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "news".
 *
 * @property int $id
 * @property string $title Заголовок статьи
 * @property string $text Текст статьи
 * @property integer $image_id Id изображения
 * @property string $link_name Адрес
 * @property int $create_at Создано
 * @property int $update_at Изменено
 */
class News extends ActiveRecord
{

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'news';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['text', 'title'], 'required'],
            [['text', 'link_name'], 'string'],
            [['image_id', 'create_at', 'update_at'], 'integer'],
            [['title', 'link_name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'title' => Yii::t('app', 'Заголовок статьи'),
            'text' => Yii::t('app', 'Текст статьи'),
            'image_id' => Yii::t('app', 'Изображение'),
            'link_name' => Yii::t('app', 'Английское название ссылки'),
            'create_at' => Yii::t('app', 'Создано'),
            'update_at' => Yii::t('app', 'Изменено'),
        ];
    }

    /**
     * @return string
     */
    public function getImage() {

        $image = Images::find()->where(['id' => $this->image_id])->one();

        if ($image) {
            return '/uploads' . $image->path .'/'. $image->name .'.'. $image->ext;
        } else {
            return false;
        }
    }
}

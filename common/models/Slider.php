<?php

namespace common\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "slider".
 *
 * @property int $id
 * @property string $title
 * @property string $text
 * @property string $short_title
 * @property int $img_id
 * @property string $btn_link
 * @property boolean $slide_position
 * @property integer $created_at
 * @property integer $updated_at
 */
class Slider extends ActiveRecord
{
    const SCENARIO_UPDATE = 'update';

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'slider';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['text', 'slide_position', 'title', 'short_title'], 'required'],
            [['text'], 'string'],
            [['img_id','created_at', 'updated_at'], 'integer'],
            [['slide_position'], 'boolean'],
            [['title', 'short_title', 'btn_link'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'title' => Yii::t('app', 'Заголовок'),
            'text' => Yii::t('app', 'Текст'),
            'short_title' => Yii::t('app', 'Короткий заголовок'),
            'img_id' => Yii::t('app', 'Изображение'),
            'slide_position' => Yii::t('app', 'Позиция слайда'),
            'btn_link' => Yii::t('app', 'Ссылка кнопки (Необязательно)'),
            'created_at' => Yii::t('app', 'Создано'),
            'updated_at' => Yii::t('app', 'Изменено')
        ];
    }

    /**
     * @return string
     */
    public function getImage() {

        $image = Images::find()->where(['id' => $this->img_id])->one();

        if ($image) {
            return '/uploads' . $image->path .'/'. $image->name .'.'. $image->ext;
        } else {
            return false;
        }
    }
}

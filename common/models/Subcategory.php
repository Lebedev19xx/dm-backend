<?php

namespace common\models;

use kcfinder\text;
use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "subcategory".
 *
 * @property int $id
 * @property string $name
 * @property string $url_name
 * @property int $main_category_id
 * @property integer $sort
 * @property integer $img_id
 * @property text $text
 */
class Subcategory extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'subcategory';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['main_category_id', 'sort'], 'integer'],
            [['name', 'url_name'], 'string', 'max' => 255],
            [['text'], 'string'],
            [['name', 'url_name'], 'required'],
            [['url_name'], 'unique']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Имя'),
            'url_name' => Yii::t('app', 'Имя в адресной строке'),
            'main_category_id' => Yii::t('app', 'Родительская категоря'),
            'sort' => Yii::t('app', 'Порядок сортировки'),
            'text' => Yii::t('app', 'текст'),
        ];
    }
}

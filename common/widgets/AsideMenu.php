<?php
/**
 * Created by PhpStorm.
 * User: dmitrii
 * Date: 26.04.19
 * Time: 13:43
 */
namespace common\widgets;

use Yii;
use yii\base\Widget;
use yii\helpers\Html;


/**
 * Виджет рендерит левое меню веб шаблона Metronic
 * Пример:
 *
 *  $menuItems = [
 *
    ['label' => 'Home', 'url' => '/site/index'],

    ['section' => 'Link'],
    ['label' => 'Link', 'url' => '/site/index', 'icon' => 'Shirt'],
    [
        'label' => 'dropdown',
        'items' => [
            ['label' => 'haha', 'url' => '/site/index'],
            [
                'label' => 'Yahhh',
                'items' => [
                    ['label' => 'third', 'url' => '/site/index'],
                ]
            ]
        ]
    ],
    ['label' => 'Next', 'url' => '/site/index'],

    ];

    echo \backend\widgets\AsideMenu::widget([
    'menuItems' => $menuItems
    ]);
 *
 *
 * asideMenu
 *
 * @author Lebedev Dmitry
 * @email d.lebedev.dev@gmail.com
 */
class AsideMenu extends Widget
{
    public $menuItems = [];

    private $content;
    private $label;
    private $icon;
    private $itemsList;
    private $dropDowns;
    private $url;
    private $innerDropDowns;

    /**
     * Рендер обертки меню
     *
     * @return string
     * @throws \Exception
     */
    private function renderMenu()
    {
       return
           Html::tag('div',
            Html::tag('div',
             Html::tag('ul', $this->renderItems(), ['class' => 'kt-menu__nav']),
               [
                   'class' => 'kt-aside-menu', 'id' => 'kt_aside_menu',
                   'data-ktmenu-vertical' => 1, 'data-ktmenu-scroll' => 1, 'data-ktmenu-dropdown-timeout' => 500
               ]),
           ['class' => 'kt-aside-menu-wrapper kt-grid__item kt-grid__item--fluid', 'id' => 'kt_aside_menu_wrapper']);
    }


    /**
     * Рендерим основные элементы меню по полученным данным
     *
     * @throws \Exception
     */
    private function renderItems()
    {

        /**
         * Разбираем полученный массив с элементами
         */
        foreach ($this->menuItems as $item) {

            // Смотрим заданна ли иконка
            if (isset($item['icon'])) $this->icon = $item['icon']; else $this->icon = 'default';

            // Проверить установлена ли секция
            if (isset($item['section'])) {
                $this->content .= $this->renderSection($item['section']);

                // Проверить наличие вложенных элементов меню
            } elseif (isset($item['items'])) {

                $this->label = $item['label'];
                $this->dropDowns = $item['items'];
                $this->content .= $this->renderInnerItems();
            } else {
                $this->label = $item['label'];
                $this->url = $item['url'];
                $this->content .= $this->generateLink();
            }

        }


        return $this->content;
    }

    /**
     * Генерируем секцию меню
     *
     * @param $name
     * @return string
     */
    private function renderSection($name)
    {
        return Html::tag('li',
                Html::tag('h4', $name, ['class' => 'kt-menu__section-text']).
                Html::tag('i', '', ['class' => 'kt-menu__section-icon flaticon-more-v2'])
            , ['class' => 'kt-menu__section']);
    }

    /**
     * Генерируем вложенные элементы меню
     *
     * @throws \Exception
     */
    private function renderInnerItems()
    {
        return
            Html::tag('li',
                $this->renderLinkToggle().
                $this->renderSubMenu(),
                [
                'class' => 'kt-menu__item  kt-menu__item--submenu',
                'aria-haspopup' => true,
                'data-ktmenu-submenu-toggle' => 'hover'
            ]);
    }


    /**
     * Генерирует subMenu и вложенные ссылки первого уровня
     */
    private function renderSubMenu(){
        return
            Html::tag('div',
            Html::tag('span', '', ['class' => 'kt-menu__arrow']).
            Html::tag('ul',
                $this->renderParentItem().
                $this->generateLinkList(),

                ['class' => 'kt-menu__subnav']),

            ['class' => 'kt-menu__submenu']);
    }

    /**
     * Li with dropDown
     *
     * @return string
     * @throws \Exception
     */
    private function renderLinkToggle()
    {
        return
        Html::a(
            Html::tag('span', SVGIconsWidget::widget(['iconName' => $this->icon]),
                ['class' => 'kt-menu__link-icon']).
                Html::tag('span', $this->label, ['class' => 'kt-menu__link-text']).
                Html::tag('i', '', ['class' => 'kt-menu__ver-arrow la la-angle-right']),
            'javascript:;', ['class' => 'kt-menu__link kt-menu__toggle']);
    }

    /**
     * Генерирует ссылку меню
     *
     * @throws \Exception
     */
    private function generateLink()
    {
        if ($this->url === ('/'.Yii::$app->controller->id . '/'. Yii::$app->controller->action->id)){
            $class = 'kt-menu__item--active';
        } else {
            $class = '';
        }
        return
            Html::tag('li',

                Html::a(
                    Html::tag('span',
                        SVGIconsWidget::widget(['iconName' => $this->icon]), ['class' => 'kt-menu__link-icon']).
                    Html::tag('span', $this->label, ['class' => 'kt-menu__link-text']),
                    $this->url,
                    ['class' => 'kt-menu__link']),

                [
                    'class' => 'kt-menu__item ' . $class, /*kt-menu__item--active*/
                    'aria-haspopup' => true
                ]);
    }

    /**
     * Генерирует список ссылок (Dropdown)
     * @return string
     */
    private function generateLinkList()
    {

        foreach ($this->dropDowns as $item) {

            if (isset($item['items'])){
//                $this->innerDropDowns = $item['items'];
//                $this->itemsList .= $this->innerSubMenu();
            } else {
                $this->itemsList .= Html::tag('li',
                    Html::a(
                        Html::tag('i',
                            Html::tag('span'),
                            ['class' => 'kt-menu__link-bullet kt-menu__link-bullet--dot']).
                        Html::tag('span', $item['label'], ['class' => 'kt-menu__link-text']),

                        $item['url'], ['class' => 'kt-menu__link']),
                    ['class' => 'kt-menu__item', 'aria-haspopup' => true]);
            }
        }

        return $this->itemsList;
    }

    /**
     * Генерирует родительский элемент ссылки
     * @return string
     */
    private function renderParentItem()
    {
        return
            Html::tag('li',
                Html::tag('span',
                    Html::tag('span', $this->label, ['class' => 'kt-menu__link-text']),
                    ['class' => 'kt-menu__link']),
                ['class' => 'kt-menu__item  kt-menu__item--parent', 'aria-haspopup' => true]);
    }

    /**
     * Запустить виджет
     * @return string
     * @throws \Exception
     */
    public function run()
    {
        return $this->renderMenu();


//        $content = '';
//
//        foreach ($this->menuItems as $item) {
//
//            if (isset($item['icon'])) $icon = $item['icon']; else $icon = 'default';
//
//            if (isset($item['section'])) {
//                $content .= '<li class="kt-menu__section ">
//									<h4 class="kt-menu__section-text">'.$item['section'].'</h4>
//									<i class="kt-menu__section-icon flaticon-more-v2"></i>
//								</li>';
//            } elseif(isset($item['items'])) {
//                $content .= '<li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover">
//									<a href="javascript:;" class="kt-menu__link kt-menu__toggle">
//										<span class="kt-menu__link-icon">
//											'.SVGIconsWidget::widget(['iconName' => 'Tie']).'
//										</span>
//										<span class="kt-menu__link-text">'.$item['label'].'</span>
//										<i class="kt-menu__ver-arrow la la-angle-right"></i>
//									</a>
//									<div class="kt-menu__submenu ">
//									    <span class="kt-menu__arrow"></span>
//										<ul class="kt-menu__subnav">
//										    <li class="kt-menu__item  kt-menu__item--parent" aria-haspopup="true">
//                                                <span class="kt-menu__link">
//                                                    <span class="kt-menu__link-text">'.$item['label'].'</span>
//                                                </span>
//											</li>';
//
//                foreach ($item['items'] as $dropDown) {
//
//                    if (isset($dropDown['items'])){
//                        $content .= '<li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover">
//                                        <a href="javascript:;" class="kt-menu__link kt-menu__toggle">
//                                            <i class="kt-menu__link-bullet kt-menu__link-bullet--dot">
//                                            <span></span>
//                                            </i>
//                                            <span class="kt-menu__link-text">'.$dropDown['label'].'</span>
//                                            <i class="kt-menu__ver-arrow la la-angle-right"></i>
//                                        </a>
//                                        <div class="kt-menu__submenu ">
//                                        <span class="kt-menu__arrow"></span>
//                                            <ul class="kt-menu__subnav">';
//
//
//                        foreach ($dropDown['items'] as $secondDropDown){
//                            $content .=  '<li class="kt-menu__item " aria-haspopup="true">
//                                                    <a href="'.$secondDropDown['url'].'" class="kt-menu__link ">
//                                                        <i class="kt-menu__link-bullet kt-menu__link-bullet--dot">
//                                                        <span></span>
//                                                        </i>
//                                                        <span class="kt-menu__link-text">'.$secondDropDown['label'].'</span>
//                                                    </a>
//                                                </li>';
//                        }
//
//                        $content  .=  '</ul>
//                                        </div>
//                                    </li>';
//
//                    } else {
//                        $content .= '<li class="kt-menu__item " aria-haspopup="true">
//                                    <a href="'.$dropDown['url'].'" class="kt-menu__link ">
//                                        <i class="kt-menu__link-bullet kt-menu__link-bullet--dot">
//                                            <span></span>
//                                        </i>
//                                        <span class="kt-menu__link-text">'.$dropDown['label'].'</span>
//                                    </a>
//                                </li>';
//                    }
//
//
//                }
//            } else {
//
//                $content .= '<li class="kt-menu__item  kt-menu__item--active" aria-haspopup="true">
//									<a href="'.$item['url'].'" class="kt-menu__link ">
//										<span class="kt-menu__link-icon">
//											'.SVGIconsWidget::widget(['iconName' => $icon]).'
//										</span>
//										<span class="kt-menu__link-text">'.$item['label'].'</span>
//									</a>
//								</li>';
//            }
//
//
//        }
//
//        $end = '</ul>
//                </div>
//            </div>';
//
//
//        // Соберем готовое меню
//        return $head . $content . $end;
    }
}
<?php


namespace common\widgets;


use common\models\MainMenuCategory;
use common\models\Subcategory;
use yii\bootstrap4\Widget;

class CategoryMenu extends Widget
{
    public $name;

    /**
     * @return string|void
     */
    public function run()
    {
        $mainCategory = MainMenuCategory::find()->where(['name' => $this->name])->one();
        $model = Subcategory::find()
            ->where(['main_category_id' => $mainCategory->id])
            ->orderBy(['sort' => SORT_DESC])
            ->all();

        if ($this->name == 'Обувь') {
            $this->renderShoesMenu($model);
        } else {
            $this->renderMenu($model);
        }
    }


    /**
     * @param $model
     */
    public function renderShoesMenu($model)
    {
        $i = 0;
        foreach ($model as $item) {

            if ($i == 8 || $i == 16) {
                echo '</div>';
            }

            if ($i == 0 || $i == 8 || $i == 16) {
                echo '<div class="col-md-4">';
            }

            echo '<a class="dropdown-item" href="/catalog/'.$item->url_name.'">'.$item->name.'</a>';


            $i++;
        }
    }

    /**
     * @param $model
     */
    public function renderMenu($model)
    {
        foreach ($model as $item) {
            echo '<a class="dropdown-item" href="/catalog/'.$item->url_name.'">'.$item->name.'</a>';
        }
    }

}
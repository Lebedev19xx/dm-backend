<?php


namespace common\widgets;


use backend\models\WebstatVisitor;
use yii\base\Widget;

class StatSevenDaysWidget extends Widget
{

    /**
     * @return string|void
     */
    public function run()
    {
        $data = WebstatVisitor::find()
            ->where(['<', 'UNIX_TIMESTAMP(created_at)', time()])
            ->andWhere(['>', 'UNIX_TIMESTAMP(created_at)', time() - (6 * 86400)])
            ->asArray()
            ->orderBy([
                'ip_address' => SORT_ASC,
                'created_at' => SORT_ASC
            ])
            ->all();

        $ip_address = '';
        $time = '';
        foreach ($data as $key => $datum) {

            // Посмотрим дату текущей записи и сохраним ее в массив
            $current_time = explode(' ', $datum['created_at']);
            $data[$key]['created_at'] = $current_time[0];

            if ($time == $current_time[0] && $ip_address == $datum['ip_address']) {
                unset($data[$key]);
            }

            // Запишем дату для сравнения со следующей записью
            $time_arr = explode(' ', $datum['created_at']);
            $time = $time_arr[0];
            $ip_address = $datum['ip_address'];
        }

        // Сортируем массив по дате
        usort($data, function($a, $b){
            return (strtotime($a['created_at']) - strtotime($b['created_at']));
        });

        // Собираем массив с датами за последние 7 дней
        $dates = [];
        for ($i = 1; $i <= 7; $i++) {
            $dates[$i - 1] = date('Y-m-d', (time() - (86400 * ($i-1))));
        }

        $dates = array_reverse($dates);

        // Подготовим массив для статистики
        $final_data = [];
        $time = 0;
        $i = 1;
        foreach ($data as $datum) {

            if (strtotime($datum['created_at']) === $time) {
                $i++;
                $final_data[$datum['created_at']] = $i;
            } else {
                $final_data[$datum['created_at']] = $i = 1;
            }
            $time = strtotime($datum['created_at']);
        }


        // Меняем значения с ключами местами
        $dates = array_flip($dates);

        // Обнуляем значения массива
        $arr_result = [];
        foreach ($dates as $key => $date) {
            $arr_result[$key] = 0;
        }

        // Объединяем массив
        $array = array_merge($arr_result, $final_data);

        $data = [];
        $valueD = [];
        foreach ($array as $key => $value) {
            $data[] = $key;
            $valueD[] = $value;
        }



        $data = json_encode($data);
        $value = json_encode($valueD);

        echo '
         <script src="https://cdn.jsdelivr.net/npm/apexcharts"></script>

            <div style="width: 100%">
                <div id="chart"></div>
            </div>

            <script>
                var data = ' .$data. '
                var value = '. $value.'

                var options = {
                    chart: {
                        width: "100%",
                        height: 380,
                        type: \'area\'
                    },

                    series: [{
                        name: \'Посетителей\',
                        data: value
                    }],
                    xaxis: {
                        categories: data
                    }
                };

                var chart = new ApexCharts(document.querySelector("#chart"), options);

                chart.render();
            </script>';
    }

}
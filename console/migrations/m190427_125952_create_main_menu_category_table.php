<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%main_menu_category}}`.
 */
class m190427_125952_create_main_menu_category_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%main_menu_category}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255)
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%main_menu_category}}');
    }
}

<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%news}}`.
 */
class m190430_130820_create_news_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%news}}', [
            'id' => $this->primaryKey(),
            'title' => $this->string(255)->comment('Заголовок статьи'),
            'text' => $this->text()->comment('Текст статьи'),
            'image_id' => $this->integer()->comment('id_img')->null(),
            'create_at' => $this->integer()->comment('Создано'),
            'update_at' => $this->integer()->comment('Изменено'),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%news}}');
    }
}

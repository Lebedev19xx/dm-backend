<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%tovar}}`.
 */
class m190508_043539_create_tovar_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%tovar}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255),
            'short_name' => $this->string(255),
            'subcategory_name' => $this->string(255),
            'heels' => $this->string(255)->null(),
            'backdrop' => $this->string(255)->null(),
            'strap' => $this->string(255)->null(),
            'buckle' => $this->string(255)->null(),
            'size' => $this->string(255)->null(),
            'color' => $this->string(255)->null(),
            'color_at_line' => $this->string(255)->null(),
            'atricul' => $this->string(255)->null(),
            'model' => $this->string(255)->null(),
            'composition' => $this->string(255)->null(),
            'material' => $this->string(255)->null(),
            'description' => $this->text()->null(),
            'link_name' => $this->string(255),
            'img_id' => $this->integer(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%tovar}}');
    }
}

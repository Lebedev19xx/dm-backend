<?php

use yii\db\Migration;

/**
 * Class m190519_184451_add_calumn_to_news_table
 */
class m190519_184451_add_calumn_to_news_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('news', 'link_name', $this->string(255)->defaultValue(null));

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('news', 'link_name');
    }
}

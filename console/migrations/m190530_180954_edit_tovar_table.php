<?php

use yii\db\Migration;

/**
 * Class m190530_180954_edit_tovar_table
 */
class m190530_180954_edit_tovar_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropColumn('tovar', 'heels');
        $this->dropColumn('tovar', 'backdrop');
        $this->dropColumn('tovar', 'strap');
        $this->dropColumn('tovar', 'buckle');
        $this->dropColumn('tovar', 'size');
        $this->dropColumn('tovar', 'color');
        $this->dropColumn('tovar', 'color_at_line');
        $this->dropColumn('tovar', 'atricul');
        $this->dropColumn('tovar', 'model');
        $this->dropColumn('tovar', 'composition');
        $this->dropColumn('tovar', 'material');
        $this->addColumn('tovar', 'item_values', $this->text());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {

    }
}

<?php

use yii\db\Migration;

/**
 * Class m190606_174232_edit_subcategory_table
 */
class m190606_174232_edit_subcategory_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('subcategory', 'sort', $this->integer(4)->null());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('subcategory', 'sort');
    }
}

<?php

use yii\db\Migration;

/**
 * Class m190606_175100_edit_tovar_table
 */
class m190606_175100_edit_tovar_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('tovar', 'sort', $this->integer(4)->null());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('tovar', 'sort');
    }
}

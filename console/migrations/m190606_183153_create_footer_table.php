<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%footer}}`.
 */
class m190606_183153_create_footer_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%footer}}', [
            'id' => $this->primaryKey(),
            'text' => $this->text(),
            'img_id' => $this->integer()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%footer}}');
    }
}

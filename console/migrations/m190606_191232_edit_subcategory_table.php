<?php

use yii\db\Migration;

/**
 * Class m190606_191232_edit_subcategory_table
 */
class m190606_191232_edit_subcategory_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('subcategory', 'text', $this->text()->null());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
      $this->dropColumn('subcategory', 'text');
    }
}

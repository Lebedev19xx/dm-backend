<?php


namespace frontend\controllers;

use common\models\Subcategory;
use common\models\Tovar;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

class CatalogController extends Controller
{

    public function actionIndex($name)
    {
        $modelSubcategory = Subcategory::find()->where(['url_name' => $name])->one();

        // Если категория не найдена, ищем в товарах
        if ($modelSubcategory) {
            $model = Tovar::find()
                ->where(['subcategory_name' => $modelSubcategory->name])
                ->orderBy(['sort' => SORT_DESC])
//                ->orderBy(['sort' => SORT_DESC])
                ->all();


            return $this->render('index', compact('model', 'modelSubcategory'));
        } else {

            $model = Tovar::find()
                ->where(['link_name' => $name])->one();

            if (!$model) throw new NotFoundHttpException('Страница не найдена');
            $modelSubcategory = Subcategory::find()->where(['name' => $model->subcategory_name])->one();


            return $this->render('view', compact('model', 'modelSubcategory'));
        }
    }
}
<?php


namespace frontend\controllers;

use common\models\News;
use yii\web\Controller;

class NewsController extends Controller
{

    public function actionIndex($name = null)
    {
        if ($name){
            return $this->render('view');
        } else {

            $news = News::find()->orderBy(['create_at' => SORT_DESC])->all();
            return $this->render('index', compact('news'));
        }
    }
}
<?php
use common\models\Tovar;
use yii\bootstrap4\Breadcrumbs;
$this->params['breadcrumbs'][] = $modelSubcategory->name;
?>
<div class="catalog-container container">
    <?= Breadcrumbs::widget([
        'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
    ])?>
    <div class="catalog-header-text d-flex">
        <h4><?= $modelSubcategory->name?></h4>
        <p><?= count($model)?> моделей</p>
    </div>
    <div class="catalog-items-container">
        <?php

        if ($model):?>
            <?php /** @var Tovar $item */
            foreach ($model as $item):?>
                <div class="card card-item" style="width: 15rem;">
                    <img src="<?= $item->getImage()?>" class="card-img-top" alt="...">
                    <div class="card-body card-item-body">
                        <h5 class="card-title"><?= $item->short_name?></h5>
                        <p class="card-text"><?= mb_substr($item->description, 0, 90)?> </p>
                        <a class="view-item" href="<?= $item->link_name?>">Подробнее
                            <i class="fal fa-angle-right"></i>
                        </a>
                    </div>
                </div>

            <?php endforeach;?>
        <?php endif;?>
    </div>
    <style>
        .other-images img{
            width: 100%;
        }
    </style>
    <div class="other-images">
        <?= $modelSubcategory->text?>
    </div>
</div>


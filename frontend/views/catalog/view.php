<?php

$this->params['breadcrumbs'][] = ['label' => Yii::t('app', $model->subcategory_name), 'url' => '/catalog/'.$modelSubcategory->url_name];
$this->params['breadcrumbs'][] = $model->short_name;


use common\models\Tovar;
use yii\bootstrap4\Breadcrumbs;

/** @var Tovar $model */
?>
<div class="item-view-container container">
    <?= Breadcrumbs::widget([
        'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
    ])?>

        <div class="item-view-wrapper row">
            <div class="main-view-item-wrapper d-flex">
                <div class="photo-view-item col-md-5" style="padding: 0;">
                    <a href="<?= $model->getImage()?>">
                        <img src="<?= $model->getImage()?>" width="100%" alt="">
                    </a>
                </div>

                <div class="description-view-item col-md-7">
                    <h3><?= $model->name?></h3>

                    <?= $model->item_values?>
                </div>
            </div>
            <div class="add-view-item-wrapper col-md-12 d-flex">
                <div class="item-main-description col-md-7">
                    <p><?= $model->description?></p>
                </div>
            </div>
        </div>
        <div class="other-items-container">
            <div class="row header-other">
                <div class="block-text col-sm d-flex align-items-center">
                    <span class="block-text_elem"></span>
                    <h3 class="block-text_text">Другие товары</h3>
                </div>
                <div class="block-next col-sm d-flex align-items-center justify-content-end">
<!--                    <a href="#">Перейти к каталогу-->
<!--<i class="fal fa-angle-right"></i>-->
<!--                    </a>-->
                </div>
            </div>
            <div class="row d-flex justify-content-center body-other">

                <?php /** @var Tovar $item */
                foreach ($model->getRandomlyItems() as $item):?>
                <div class="card card-item" style="width: 15rem;">
                    <img src="<?= $item->getImage()?>" class="card-img-top" alt="...">
                    <div class="card-body card-item-body">
                        <h5 class="card-title"><?= $item->short_name?></h5>
                        <p class="card-text"><?= substr($item->description, 0, 121)?> </p>
                        <a class="view-item" href="<?= $item->link_name?>">Подробнее
                            <i class="fal fa-angle-right"></i>
                        </a>
                    </div>
                </div>
                <?php endforeach;?>
            </div>
        </div>
    </div>
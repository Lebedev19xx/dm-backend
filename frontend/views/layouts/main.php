<?php

/* @var $this \yii\web\View */
/* @var $content string */

use common\widgets\CategoryMenu;
use frontend\assets\AppAsset;
use yii\helpers\Html;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,500" rel="stylesheet">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<!--[if lte IE 9]>
<p class="browserupgrade">Вы используйте <strong>устаревший</strong> браузер. Пожалуйста
    <a href="https://www.google.com/chrome/">обновите Ваш браузер</a>
</p>
<![endif]-->
<header class="container-fluid" id="header-container">
    <div class="header-container container">
        <div class="row">
            <div class="col-md search d-flex align-items-center">
                <form class="form-inline search-wrapper">
                    <input class="global-search" type="search" placeholder="Поиск..." aria-label="Search">
                    <i class="fal fa-search"></i>
                </form>
            </div>
            <div class="col-md brand d-flex justify-content-center align-items-center">
                <a href="<?= Yii::$app->homeUrl;?>">
                    <img src="/img/logo.png" width="230" alt="logo-brand">
                </a>
            </div>
            <div class="col-md language d-flex align-items-center justify-content-end">
<!--                <a href="#"><span></span> RU</a>-->
            </div>
        </div>
    </div>
    <nav class="navbar navbar-expand-md nav-container container">
        <a class="navbar-brand" href="<?= Yii::$app->homeUrl;?>">
            <img src="img/logo.png" width="160" alt="logo-brand">
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="bar topBar"></span>
            <span class="bar btmBar"></span>
        </button>

        <div class="collapse navbar-collapse justify-content-center">
            <ul class="navbar-nav justify-content-center">
                <li class="nav-item">
                    <a class="nav-link active" href="<?= Yii::$app->homeUrl;?>">Главная</a>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-link" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
                        Обувь
                        <i class="fal fa-angle-down"></i>
                    </a>
                    <div class="dropdown-menu dropdown-container">
                        <div class="row no-gutters">
                            <?= CategoryMenu::widget(['name' => 'Обувь'])?>
                        </div>
                    </div>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-link" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
                        Одежда
                        <i class="fal fa-angle-down"></i>
                    </a>
                    <div class="dropdown-menu">
                        <?= CategoryMenu::widget(['name' => 'Одежда'])?>
                    </div>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-link" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
                        Галантерея
                        <i class="fal fa-angle-down"></i>
                    </a>
                    <div class="dropdown-menu">
                        <?= CategoryMenu::widget(['name' => 'Галантерея'])?>
                    </div>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/catalog/accessories">Аксессуары</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/catalog/awards">Награды</a>
                </li>
<!--                <li class="nav-item">-->
<!--                    <a class="nav-link" href="#">Турниры</a>-->
<!--                </li>-->
                <li class="nav-item">
                    <a class="nav-link" href="/news/">Новости</a>
                </li>
            </ul>
        </div>
    </nav>
</header>
<div class="mobile-menu-container container display-none">
    <ul class="ul-mobile">

    </ul>
</div>
<main>
    <?= $content?>
</main>

<footer class="footer-container container-fluid">
    <div class="container footer-wrapper d-flex">
        <div class="copy-right d-flex">
            <p>© Dancemaster-international, <?= date('Y')?> г. Все права защищены.</p>
        </div>
        <div class="web-developer d-flex">
<!--            <p>Сделано в</p>-->
<!--            <a href="#">LebedevWeb</a>-->
        </div>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>

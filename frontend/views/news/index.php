
<section class="news-container container">
    <div class="block-header-container">
        <div class="row">
            <div class="block-text col-sm d-flex align-items-center">
                <span class="block-text_elem"></span>
                <h3 class="block-text_text">Новости</h3>
            </div>
        </div>
    </div>
    <div class="news-body-container">
        <div class="row">
            <div class="col-lg-12 news-full-container page-news-container d-flex">


                <?php /** @var \common\models\News $new */
                foreach ($news as $new):?>
                <div class="card card-news col-md-4">
                    <a href="<?= $new->link_name?>">
                        <div class="card-img-wrapper d-flex align-items-center">
                            <img class="card-img-top" src="<?= $new->getImage()?>" alt="Card image cap">
                        </div>
                    </a>
                    <div class="card-body">
                        <h5 class="card-title"><?= $new->title?></h5>
                        <p class="card-text"><?= mb_substr($new->text, 0, 50)?>
                            <a href="<?= $new->link_name?>" class="card-text-link">Читать далее
                                <!--<i class="fal fa-angle-double-right icon"></i>-->
                            </a>
                        </p>
                        <p class="card-date"><?= date('d.m.Y', $new->create_at)?></p>
                    </div>
                </div>

                <?php endforeach;?>
            </div>
        </div>
    </div>
</section>
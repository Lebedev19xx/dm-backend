<?php

/* @var $this yii\web\View */

use common\models\News;
use common\models\Slider;

$this->title = 'Dancemaster';
/** @var News $item */
?>
<section class="slider-container container-fluid">
    <div id="carouselIndicators" class="carousel slide carousel-fade" data-ride="carousel">
        <ol class="carousel-indicators">
            <?php
            /** @var Slider $slider */
            $i = 0;
            foreach ($sliders as $slider):?>
                <li data-target="#carouselIndicators" data-slide-to="<?= $i?>" class="<?php if ($i == 0) echo 'active'?> slider-indicators">
                    <div class="indicator-active"></div>
                    <div class="indicator-text-wrapper">
                        <p><?= $slider->short_title?></p>
                    </div>
                </li>
            <?php $i++; endforeach;?>
        </ol>

        <div class="carousel-inner">
            <?php
            $i = 0;
            foreach ($sliders as $slider):?>

            <div class="carousel-item <?php if($i == 0) echo 'active';?>">
                <div class="slide-item-container">
                    <div class="img-side-container col-md-12 img-slider-full" style="padding: 0">
                        <img class="d-block" src="<?= $slider->getImage()?>" alt="slide">
                        <div class="carousel-caption d-none d-md-block slide-text-wrapper">
                            <h4><?= $slider->title?></h4>
                            <p><?= $slider->text?></p>
                        </div>
                    </div>
                </div>
            </div>

            <?php $i++; endforeach;?>
        </div>
        <a class="carousel-control-prev" href="#carouselIndicators" role="button" data-slide="prev">
            <div class="carousel-control-btn">
                <i class="fal fa-angle-left-white"></i>
            </div>
        </a>
        <a class="carousel-control-next" href="#carouselIndicators" role="button" data-slide="next">
            <div class="carousel-control-btn">
                <i class="fal fa-angle-right-white"></i>
            </div>
        </a>
    </div>
</section>

<!-- News block-->
<section class="news-container container">
    <div class="block-header-container">
        <div class="row">
            <div class="block-text col-sm d-flex align-items-center">
                <span class="block-text_elem"></span>
                <h3 class="block-text_text">Последние новости</h3>
            </div>
            <div class="block-next col-sm d-flex align-items-center justify-content-end">
                <a href="#">Перейти к другим новостям
                    <!--<i class="fal fa-angle-right"></i>-->
                </a>
            </div>
        </div>
    </div>
    <div class="news-body-container">
        <div class="row">
            <?php
            $i = 0;
            foreach ($news as $item):?>
            <?php if ($i == 0):?>
            <div class="col-lg-8 news-full-container d-flex justify-content-around">
            <?php endif;?>

            <?php if ($i < 2):?>
                <div class="card card-news col-md-6">
                    <a href="<?= $item->link_name?>">
                        <div class="card-img-wrapper d-flex align-items-center">
                            <img class="card-img-top" src="<?= $item->getImage()?>" alt="Card image cap">
                        </div>
                    </a>
                    <div class="card-body">
                        <h5 class="card-title"><?= $item->title?></h5>
                        <p class="card-text"><?= mb_substr($item->text, 0, 50)?>
                            <a href="<?= $item->link_name?>" class="card-text-link">Читать далее
                                <!--<i class="fal fa-angle-double-right icon"></i>-->
                            </a>
                        </p>
                        <p class="card-date">Опубликовано <?= date('d.m.Y', $item->create_at)?></p>
                    </div>
                </div>
            <?php endif;?>

            <?php if ($i == 1):?>
            </div>
            <?php endif;?>

            <?php if ($i == 2):?>
            <div class="col-lg-4 news-short-container">
            <?php endif;?>

            <?php if ($i > 1):?>
                <div class="news-short-item">
                    <a href="<?= $item->link_name?>"><?= $item->title?></a>
                    <p class="news-short-date">Опубликовано <?= date('d.m.Y', $item->create_at)?></p>
                </div>
            <?php endif;?>

            <?php if ($i == 5):?>
            </div>
            <?php endif;?>
            <?php $i++; endforeach;?>
        </div>
    </div>
</section>
<section class="container-about container d-flex">
    <div class="logo-about-wrapper col-md-6">
        <img src="<?= $footer->getImage()?>" alt="logo-big-brand">
    </div>
    <div class="text-about-wrapper col-md-5">
        <p><?= $footer->text?></p>
    </div>
</section>

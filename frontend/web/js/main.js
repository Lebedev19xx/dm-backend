// header fix при прокрутке
$(window).scroll(function() {
  if ($(window).width() >= '768'){
    if ($(this).scrollTop() > 87){
      $('#header-container').addClass("fixed-top");
      $('.header-container').addClass('hidden');
      $('main').addClass('padding-main');
    }
    else{
      $('#header-container').removeClass("fixed-top");
      $('.header-container').removeClass('hidden');
      $('main').removeClass('padding-main');
    }
  }else{
    // $('#header-container').addClass("fixed-top");
    // $('main, .mobile-menu-container').addClass('padding-mobile');
  }
});



// // кнопка вверх
// $(document).ready(function(){
//   $('body').append('<a href="#" id="go-top" class="go-top" title="Вверх">' +
//     '<i class="fa-angle-up icon-top"></i>' +
//     '</a>');
// });
//
// $(function() {
//   $.fn.scrollToTop = function() {
//     $(this).hide().removeAttr("href");
//     if ($(window).scrollTop() >= "87") $(this).fadeIn("slow");
//     var scrollDiv = $(this);
//     $(window).scroll(function() {
//       if ($(window).scrollTop() <= "87") $(scrollDiv).fadeOut("slow");
//       else $(scrollDiv).fadeIn("slow")
//     });
//     $(this).click(function() {
//       $("html, body").animate({scrollTop: 0}, "slow")
//     })
//   }
// });
//
// $(function() {
//   $("#go-top").scrollToTop();
// });


$(document).ready(function () {
  $('.navbar-toggler').on('click', function () {
    $('.navbar-toggler').toggleClass('menu-opened');
    $('main, .btn-menu, .mobile-menu-container').toggleClass('display-none');
    $('.nav-item').appendTo('.ul-mobile');
  });
});

// // lazy load
// $(document).ready(function () {
//     $('.lazy').Lazy();
// });

// Touch slider
$(".carousel").on("touchstart", function(event){
    var xClick = event.originalEvent.touches[0].pageX;
    $(this).one("touchmove", function(event){
        var xMove = event.originalEvent.touches[0].pageX;
        if( Math.floor(xClick - xMove) > 5 ){
            $(this).carousel('next');
        }
        else if( Math.floor(xClick - xMove) < -5 ){
            $(this).carousel('prev');
        }
    });
    $(".carousel").on("touchend", function(){
        $(this).off("touchmove");
    });
});

// // Instantiate EasyZoom instances
// var $easyzoom = $('.photo-view-item').easyZoom();
//
// // Get an instance API
// var api = $easyzoom.data('easyZoom');